This repository contains the data and R code to reproduce the article "Impacts of the 
FLEGT action plan and the EU timber regulation on EU trade in timber product" available 
at https://doi.org/10.3390/su13116030 .


# Data

- [data/results/swd.csv](data/results/swd.csv) contains bilateral time series of 
  tropical lumber imports and oak lumber imports used to estimate the shock model.

- [data/results/swd_balanced_panel.csv](data/results/swd_balanced_panel.csv) contains a 
  balanced panel dataset of tropical lumber and oak lumber imports.


# Econometric analysis and plots

The pdf version of the article can be generated from the 
[makefile](manuscript/makefile). There is also a [.gitlab-ci.yml](.gitlab-ci.yml) 
configuration file to build the manuscript on a continuous integration platform. It 
depends on two different docker containers providing a R and a Latex environnement.


