
# source of metadata

The metadata files  comext_units_su.txt  product_names_hs44.csv  README.md
vpa_partners.csv

were exported from the tradeflows database. 
The tradeflows database is populated with data from the Comext bulk download
facility. Deep link to the metadata: 
https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?sort=1&dir=comext%2FCOMEXT_METADATA%2FCLASSIFICATIONS_AND_RELATIONS%2FENGLISH


# source of CN changes
CN Update of codes: synoptic table presenting all CN changes 
from one year to the other since 1988 
Comes from
https://ec.europa.eu/eurostat/ramon/relations/index.cfm?TargetUrl=LST_REL
Direct link to the zip archive: 
http://ec.europa.eu/eurostat/ramon/documents/cn_2019/CN_2019_UPDATE_SINCE_1988.zip

