#' Capitalise every first letter of a word and replace underscores by spaces
#' @param s character vector
#' @return character vector the same length as the input s
pretty_column_names <- function(s){
     # Sub function for one vector element
     cap <- function(s){
         paste(toupper(substring(s, 1, 1)), substring(s, 2), 
               sep = "", collapse = " ")
     } 
     # Apply cap to all vector elements
     sapply(strsplit(s, split = "_"), cap, USE.NAMES = !is.null(names(s)))
}

# Add ratio weight_trop / weight_oak as a comparison indicator
#' @param df data frame containing period, weight_t and weight_k columns
#' @example compute_ratio_t_over_k(coint_shock_log$data[[1]])
compute_ratio_t_over_k <- function(df, t = "weight_t", k = "weight_k", min_period = "2010-01-01"){
    df <- df[df$period > min_period,]
    sum(df[t]) / sum(df[k])
}

